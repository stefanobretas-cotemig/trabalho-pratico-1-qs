------------------------------SPRINT 1
-- CRIACAO DO BANCO
use master
drop database HomePetsDb

CREATE DATABASE HomePetsDb;
use HomePetsDb;

CREATE TABLE Usuario(
	Id uniqueidentifier not null,
	Nome varchar(50) not null,
	Cpf varchar(11) not null,
	Bairro varchar(50) not null,
	Cidade varchar(50) not null,
	Email varchar(30) not null,
	Telefone varchar(12) not null,
	TipoUsuario int not null,
	PRIMARY KEY(Id)
);
GO
CREATE TABLE Pet(
	Id uniqueidentifier not null,
	Nome varchar(30) not null,
	TipoPet varchar(20) not null,
	Raca varchar(30) not null,
	Observacao varchar(240) null,
	IdUsuario uniqueidentifier not null,
	PRIMARY KEY(Id),
	FOREIGN KEY(IdUsuario) REFERENCES Usuario(Id)
);
GO
CREATE TABLE AgendaPasseio(
	Id uniqueidentifier not null,
	DataHorario datetime not null,
	AgendaAceita bit not null,
	IdPet uniqueidentifier not null,
	PRIMARY KEY(Id),
	FOREIGN KEY(IdPet) REFERENCES Pet(Id),
);
GO
CREATE TABLE Passeio(
	Id uniqueidentifier not null,
	TempoPasseio int not null,
	Nota int not null,
	ValorTotal float null,
	PasseioPago bit not null default 0,
	IdAgenda uniqueidentifier not null,
	IdPasseador uniqueidentifier not null,
	PRIMARY KEY(Id),
	FOREIGN KEY(IdAgenda) REFERENCES AgendaPasseio(Id),
	FOREIGN KEY(IdPasseador) REFERENCES Usuario(Id),
);
GO

-------INSERTS

INSERT INTO Usuario values ('9651ceb4-7ec2-4e06-907c-a9f57a26f286', 'Leonardo', '51007531029', 'Padre Eustaquio', 'Belo Horizonte', 'leonardo@gmail.com', '996398527', 2);

INSERT INTO Usuario values ('69f212af-cea0-4f66-b96b-2f109fd7369a', 'Lucas', '51007531029', 'Castelo', 'Belo Horizonte', 'Lucas@gmail.com', '9925874196', 1);

INSERT INTO Pet values ('df5b77b9-234d-4c18-b078-0125d771ab38', 'juju', 'Cachorro', 'vira lata', '', '69f212af-cea0-4f66-b96b-2f109fd7369a');

INSERT INTO AgendaPasseio values ('8dd16988-9b7f-4b87-b298-06ea578c9289', '20120618 10:30:00 AM', 0, 'df5b77b9-234d-4c18-b078-0125d771ab38');


--------NOVOS INSERTS 

---DONOS DE PETS

INSERT INTO Usuario values ('5502aff1-342a-467b-b6b4-386e286f0d47', 'Pedro', '51007531029', 'Barroca', 'Belo Horizonte', 'pedro@gmail.com', '970428189', 1);

INSERT INTO Usuario values ('ed8eb33a-80e7-4940-a310-68e4ffaf00ca', 'Vicente', '51007531029', 'Nova Suiça', 'Belo Horizonte', 'vicente@gmail.com', '946578620', 1);

INSERT INTO Usuario values ('3be0c977-3571-4d74-9fde-7986f5255f47', 'Ian', '51007531029', 'Alípio de Melo', 'Belo Horizonte', 'ian@gmail.com', '909650097', 1);

INSERT INTO Usuario values ('1c61e90f-b223-490f-b6ad-51413e711f16', 'Luigi', '51007531029', 'Castelo', 'Belo Horizonte', 'luigi@gmail.com', '967860239', 1);

---PASSEADORES

INSERT INTO Usuario values ('371ca4e1-66f9-4969-9562-a0c24de3ffbc', 'Pedro Henrique', '51007531029', 'Pampulha', 'Belo Horizonte', 'pedrohenrique@gmail.com', '962989193', 2);

INSERT INTO Usuario values ('728416fc-dd74-40d3-9ba8-7446071da125', 'Atila', '51007531029', 'Betania', 'Belo Horizonte', 'atila@gmail.com', '956976706', 2);

INSERT INTO Usuario values ('22e2d8d4-3433-4a1f-8189-5ce50cf15781', 'Gabriela', '51007531029', 'Floresta', 'Belo Horizonte', 'gabriela@gmail.com', '969246110', 2);

INSERT INTO Usuario values ('9fdcbcc2-25ee-498d-bd91-68f8d5ced579', 'Luana', '51007531029', 'Buritis', 'Belo Horizonte', 'luana@gmail.com', '971708851', 2);


---PETS

INSERT INTO Pet values ('8cdd0968-d1f9-422c-b93b-8807d00905db', 'luke', 'Cachorro', 'Pug', '', '5502aff1-342a-467b-b6b4-386e286f0d47');

INSERT INTO Pet values ('d2b84a2e-7cb0-437a-8a5b-dc67ee18dbdf', 'mike', 'Cachorro', 'Golden', '', 'ed8eb33a-80e7-4940-a310-68e4ffaf00ca');

INSERT INTO Pet values ('e67c7e7f-1fb8-439c-bb51-4d8355614625', 'thor', 'Cachorro', 'Pastor alemão', '', '3be0c977-3571-4d74-9fde-7986f5255f47');

INSERT INTO Pet values ('8a7554cd-3773-4dfd-8b8f-0e52f5695022', 'apolo', 'Cachorro', 'Bassê', '', '1c61e90f-b223-490f-b6ad-51413e711f16');

---AGENDA PASSEIOS

INSERT INTO AgendaPasseio values ('5d35b6b9-3079-4425-82a1-321a5ffe958c', '20210518 08:30:00 AM', 0, '8a7554cd-3773-4dfd-8b8f-0e52f5695022');

INSERT INTO AgendaPasseio values ('b6e0b35d-237f-4e8e-aeb8-880875e7f6a3', '20210518 09:30:00 AM', 0, '8cdd0968-d1f9-422c-b93b-8807d00905db');

INSERT INTO AgendaPasseio values ('c93bb8ee-aea3-47bb-ae35-bcd5a585fe65', '20210518 10:30:00 AM', 0, 'd2b84a2e-7cb0-437a-8a5b-dc67ee18dbdf');

INSERT INTO AgendaPasseio values ('1201b718-b3e7-476b-94c0-8dbebfbce664', '20210518 11:30:00 AM', 0, 'e67c7e7f-1fb8-439c-bb51-4d8355614625');

INSERT INTO AgendaPasseio values ('a590e5f2-be0b-4c51-ba4b-a29c644a4003', '20210518 13:30:00 PM', 0, 'e67c7e7f-1fb8-439c-bb51-4d8355614625');

INSERT INTO AgendaPasseio values ('5eacb159-a17a-4ac4-85b7-04e1df00f9f6', '20210518 14:30:00 PM', 0, '8a7554cd-3773-4dfd-8b8f-0e52f5695022');

---PASSEIOS

INSERT INTO Passeio VALUES ('2f12e035-b1a8-4b16-bb08-2310d43faf93', 1, 5, 12.00, 0, '5d35b6b9-3079-4425-82a1-321a5ffe958c', '371ca4e1-66f9-4969-9562-a0c24de3ffbc');

INSERT INTO Passeio VALUES ('8b5d72a1-b746-458b-8af8-f5cf9feb34e1', 1, 4, 10.00, 0, 'b6e0b35d-237f-4e8e-aeb8-880875e7f6a3', '371ca4e1-66f9-4969-9562-a0c24de3ffbc');

INSERT INTO Passeio VALUES ('bfcc0656-fc4e-4c01-bec8-b7019e925c25', 1, 3, 15.00, 0, 'c93bb8ee-aea3-47bb-ae35-bcd5a585fe65', '371ca4e1-66f9-4969-9562-a0c24de3ffbc');

INSERT INTO Passeio VALUES ('cb5f73ce-f606-4554-b26d-965c7821f732', 2, 4, 24.00, 0, '1201b718-b3e7-476b-94c0-8dbebfbce664', '728416fc-dd74-40d3-9ba8-7446071da125');

INSERT INTO Passeio VALUES ('1f9b2c85-9a2a-4f2a-accc-e9000748d985', 2, 2, 24.00, 0, '8cdd0968-d1f9-422c-b93b-8807d00905db', '728416fc-dd74-40d3-9ba8-7446071da125');

INSERT INTO Passeio VALUES ('c2fcbe53-44f8-40de-8fe1-3e4dccc44ede', 2, 4, 24.00, 0, 'e67c7e7f-1fb8-439c-bb51-4d8355614625', '9fdcbcc2-25ee-498d-bd91-68f8d5ced579');



