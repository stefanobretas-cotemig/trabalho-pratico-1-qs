﻿using HomePets.WebApp.Domain.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePets.WebApp.Models
{
    public class PasseioModel
    {
        public PasseioModel() { }

        public PasseioModel(Passeio passeio)
        {
            Id = passeio.Id;
            TempoPasseio = passeio.TempoPasseio;
            IdAgenda = passeio.IdAgenda;
            Nota = passeio.Nota;
            ValorTotal = passeio.ValorTotal;
            PasseioPago = passeio.PasseioPago;
            IdPasseador = passeio.IdPasseador;
            DataHoraAgenda = passeio.Agenda.DataHorario.ToString("dd-mm-yyyy HH:mm:ss");
            NomePasseador = passeio.Passeador.Nome;
            NomePet = passeio.Agenda.Pet.Nome;
        }

        public Guid Id { get; set; }

        public Guid IdAgenda { get; set; }

        public int TempoPasseio { get; set; }

        public int Nota { get; set; }

        public double ValorTotal { get; set; }

        public bool PasseioPago { get; set; }

        public Guid IdPasseador { get; set; }

        public List<SelectListItem> Passeadores { get; set; } = new List<SelectListItem>();

        public string DataHoraAgenda { get; set; }

        public string NomePasseador { get; set; }

        public bool EstaFinalizando { get; set; }

        public string PasseioPagoString { get { return PasseioPago ? "Sim" : "Não"; } }

        public string TempoPasseioString { get { return $"{TempoPasseio}h"; } }

        public string NomePet { get; set; }
    }
}
