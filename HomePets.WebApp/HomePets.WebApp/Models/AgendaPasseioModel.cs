﻿using HomePets.WebApp.Domain.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePets.WebApp.Models
{
    public class AgendaPasseioModel
    {
        public AgendaPasseioModel() { }

        public AgendaPasseioModel(AgendaPasseio agenda)
        {
            Id = agenda.Id;
            DataHorario = agenda.DataHorario;
            AgendaAceita = agenda.AgendaAceita;
            NomePet = agenda.Pet.Nome;
            RacaPet = agenda.Pet.Raca;
        }

        public Guid? Id { get; set; }

        public DateTime DataHorario { get; set; }

        public bool AgendaAceita { get; set; }

        public Guid IdPet { get; set; }

        public List<SelectListItem> Pets { get; set; }

        public string NomePet { get; set; }

        public string RacaPet { get; set; }
    }
}
