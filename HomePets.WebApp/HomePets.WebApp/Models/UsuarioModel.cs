﻿using HomePets.WebApp.Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace HomePets.WebApp.Models
{
    public class UsuarioModel
    {
        public UsuarioModel()
        {

        }

        public UsuarioModel(Usuario usuario)
        {
            Id = usuario.Id;
            Nome = usuario.Nome;
            Cpf = usuario.Cpf;
            Bairro = usuario.Bairro;
            Cidade = usuario.Cidade;
            Email = usuario.Email;
            Telefone = usuario.Telefone;
            TipoUsuario = usuario.TipoUsuario;
        }

        public Guid? Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "O campo deve conter 11 caracteres")]
        public string Cpf { get; set; }

        [Required]
        public string Bairro { get; set; }

        [Required]
        public string Cidade { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Telefone { get; set; }

        [Required]
        public TipoUsuario TipoUsuario { get; set; }
    }
}
