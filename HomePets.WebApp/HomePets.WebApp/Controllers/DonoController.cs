﻿using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using HomePets.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePets.WebApp.Controllers
{
    public class DonoController : Controller
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public DonoController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public async Task<IActionResult> Index()
        {
            var model = new List<UsuarioModel>();
            var lista_donos = await _usuarioRepository.ListarDonos();

            if (lista_donos.Any())
            {
                foreach(var item in lista_donos)
                {
                    model.Add(new UsuarioModel(item));
                }
            }

            return View(model);
        }

        public IActionResult Create()
        {
            var model = new UsuarioModel();
            model.TipoUsuario = Domain.Models.TipoUsuario.DonoPet;
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var dono = await _usuarioRepository.ObterUsuarioPorId(id);
            var model = new UsuarioModel(dono);
            return View("Create", model);
        }

        public async Task<IActionResult> Insert(UsuarioModel model)
        {
            var sucesso = false;
            var usuario = MontarEntidade(model);

            if (model.Id != null)
                sucesso = await _usuarioRepository.AtualizarUsuario(usuario);
            else
                sucesso = await _usuarioRepository.CadastrarUsuario(usuario);

            if (sucesso)
                return RedirectToAction("Index", "DonoPet");
            else
                return View("Create", model);
        }

        private Usuario MontarEntidade(UsuarioModel model)
        {
            return new Usuario
            {
                Id = model.Id.HasValue ? model.Id.Value : Guid.NewGuid(),
                TipoUsuario = model.TipoUsuario,
                Bairro = model.Bairro,
                Cidade = model.Cidade,
                Cpf = model.Cpf,
                Email = model.Email,
                Nome = model.Nome,
                Telefone = model.Telefone
            };
        }
    }
}
