﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomePets.WebApp.Domain.Interfaces;
using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using HomePets.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace HomePets.WebApp.Controllers
{
    public class PasseadorController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IUsuarioRepository _usuarioRepository;

        public PasseadorController(IUsuarioService usuarioService, IUsuarioRepository usuarioRepository)
        {
            _usuarioService = usuarioService;
            _usuarioRepository = usuarioRepository;
        }

        public async Task<IActionResult> Index()
        {
            var model = new List<UsuarioModel>();
            var passeadores = await _usuarioService.ListarPasseadores();

            if (passeadores.Any())
            {
                foreach (var item in passeadores)
                {
                    model.Add(new UsuarioModel(item));
                }
            }

            return View(model);
        }

        public async Task<IActionResult> VisualizarPasseios(Guid id)
        {
            var model = new List<PasseioModel>();
            var passeios = await _usuarioRepository.ListarPasseiosPorPasseador(id);

            if (passeios.Any())
            {
                foreach (var item in passeios)
                {
                    model.Add(new PasseioModel(item));
                }
            }

            return View(model);
        }

        public async Task<IActionResult> AverageGrade(Guid id)
        {
            var model = await _usuarioService.ObterNotaMediaPasseador(id);
            return View("NotaMediaPasseador", model);
        }

        public IActionResult Create()
        {
            var model = new UsuarioModel();
            model.TipoUsuario = TipoUsuario.Passeador;
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var passeador = await _usuarioRepository.ObterUsuarioPorId(id);
            var model = new UsuarioModel(passeador);
            return View("Create", model);
        }

        public async Task<IActionResult> Insert(UsuarioModel model)
        {
            var sucesso = false;
            var usuario = MontarEntidade(model);

            if (model.Id != null)
                sucesso = await _usuarioRepository.AtualizarUsuario(usuario);
            else
                sucesso = await _usuarioRepository.CadastrarUsuario(usuario);

            if (sucesso)
                return RedirectToAction("Index", "Passeador");
            else
                return View("Create", model);
        }

        private Usuario MontarEntidade(UsuarioModel model)
        {
            if (ModelState.IsValid)
            {
                return new Usuario
                {
                    Id = model.Id.HasValue ? model.Id.Value : Guid.NewGuid(),
                    TipoUsuario = model.TipoUsuario,
                    Bairro = model.Bairro,
                    Cidade = model.Cidade,
                    Cpf = model.Cpf,
                    Email = model.Email,
                    Nome = model.Nome,
                    Telefone = model.Telefone
                };
            }
            else
            {
                return new Usuario();
            }
        }
    }
}
