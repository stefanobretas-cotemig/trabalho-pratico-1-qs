﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using HomePets.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HomePets.WebApp.Controllers
{
    public class PasseioController : Controller
    {
        private readonly IAgendaPasseioRepository _agendaPasseioRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IPasseioRepository _passeioRepository;

        public PasseioController(IAgendaPasseioRepository agendaPasseioRepository, IUsuarioRepository usuarioRepository, IPasseioRepository passeioRepository)
        {
            _agendaPasseioRepository = agendaPasseioRepository;
            _usuarioRepository = usuarioRepository;
            _passeioRepository = passeioRepository;
            
        }

        public async Task<IActionResult> Index()
        {
            var passeios = new List<PasseioModel>();
            var lista = await _passeioRepository.ListarPasseiosAtivos();

            foreach (var item in lista)
            {
                var passeio = new PasseioModel(item);
                passeios.Add(passeio);
            }

            return View(passeios);
        }

        public async Task<IActionResult> InserirPasseio(PasseioModel model)
        {
            var passeio = MontarEntidadeIniciarPasseio(model);
            await _passeioRepository.IniciarPasseio(passeio);

            return RedirectToAction("Index", "AgendaPasseio");
        }

        public async Task<IActionResult> AtualizarPasseioParaFinalizado(PasseioModel model)
        {
            var entidade = await _passeioRepository.ObterPasseioPorId(model.Id);
            var passeio = MontarEntidadeFinalizarPasseio(model, entidade);
            await _passeioRepository.AtualizarPasseio(passeio);

            return RedirectToAction("Index", "AgendaPasseio");
        }

        public async Task<IActionResult> FinalizarPasseio(Guid idPasseio)
        {
            //TODO: Finalizar metodo para finalizar passeio e testar
            PasseioModel model = new PasseioModel();
            var passeio = await _passeioRepository.ObterPasseioPorId(idPasseio);
            model.EstaFinalizando = true;
            model.Id = idPasseio;
            model.IdAgenda = passeio.IdAgenda;
            model.IdPasseador = passeio.IdPasseador;

            var passeadores = await _usuarioRepository.ListarPasseadores();
            foreach (var passeador in passeadores)
            {
                var item = new SelectListItem() { Text = $"{passeador.Nome}", Value = passeador.Id.ToString() };
                model.Passeadores.Add(item);
            }

            return View("IniciarPasseio", model);
        }

        public async Task<IActionResult> IniciarPasseio(Guid idAgenda)
        {
            PasseioModel model = new PasseioModel();
            model.IdAgenda = idAgenda;

            var passeadores = await _usuarioRepository.ListarPasseadores();
            foreach (var passeador in passeadores)
            {
                var item = new SelectListItem() { Text = $"{passeador.Nome}", Value = passeador.Id.ToString() };
                model.Passeadores.Add(item);
            }

            return View(model);
        }

        public async Task<IActionResult> AvaliarPasseio(Guid idPasseio)
        {
            var entidade = await _passeioRepository.ObterPasseioPorId(idPasseio);
            var model = new PasseioModel(entidade);

            return View("AvaliarPasseio", model);
        }

        public async Task<IActionResult> AtualizarPasseioComNota(PasseioModel model)
        {
            var passeio = await _passeioRepository.ObterPasseioPorId(model.Id);
            passeio.Nota = model.Nota;
            await _passeioRepository.AtualizarPasseio(passeio);

            return RedirectToAction("Index", "Passeio");
        }

        private Passeio MontarEntidadeIniciarPasseio(PasseioModel model)
        {
            var passeio = new Passeio()
            {
                IdAgenda = model.IdAgenda,
                Id = Guid.NewGuid(),
                IdPasseador = model.IdPasseador,
                Nota = 0,
                PasseioPago = false,
                ValorTotal = model.ValorTotal,
                TempoPasseio = model.TempoPasseio
            };

            return passeio;
        }

        private Passeio MontarEntidadeFinalizarPasseio(PasseioModel model, Passeio passeio)
        {
            passeio.Nota = model.Nota;
            passeio.PasseioPago = true;
            passeio.TempoPasseio = model.TempoPasseio;
            passeio.ValorTotal = model.ValorTotal;

            return passeio;
        }

    }
}
