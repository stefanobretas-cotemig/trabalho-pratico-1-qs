﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using HomePets.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HomePets.WebApp.Controllers
{
    public class AgendaPasseioController : Controller
    {
        private readonly IAgendaPasseioRepository _agendaPasseioRepository;
        private readonly IPetRepository _petRepository;
        private readonly IPasseioRepository _passeioRepository;

        public AgendaPasseioController(IAgendaPasseioRepository agendaPasseioRepository, IPetRepository petRepository, IPasseioRepository passeioRepository)
        {
            _agendaPasseioRepository = agendaPasseioRepository;
            _petRepository = petRepository;
            _passeioRepository = passeioRepository;
        }

        public async Task<IActionResult> Index()
        {
            var agendas = new List<AgendaPasseioModel>();
            var lista = await _agendaPasseioRepository.ListarAgendasAtivas();

            foreach (var item in lista)
            {
                var agenda = new AgendaPasseioModel(item);
                agendas.Add(agenda);
            }

            return View(agendas);
        }

        public async Task<IActionResult> Create()
        {
            var model = new AgendaPasseioModel();
            model.Pets = new List<SelectListItem>();

            var pets = await _petRepository.ListarPets();
            foreach (var pet in pets)
            {
                var item = new SelectListItem() { Text = $"{pet.Nome}: {pet.TipoPet}, {pet.Raca}", Value = pet.Id.ToString() };
                model.Pets.Add(item);
            }

            return View(model);
        }

        public async Task<IActionResult> Insert(AgendaPasseioModel model)
        {
            var agenda = MontarEntidade(model);
            var sucesso = await _agendaPasseioRepository.CadastrarAgenda(agenda);

            if(sucesso)
                return RedirectToAction("Index", "AgendaPasseio");
            else
                return View("Create", model);

        }

        public async Task<IActionResult> Delete(Guid id)
        {
            await _agendaPasseioRepository.ApagarAgenda(id);
            return RedirectToAction("Index", "AgendaPasseio");
        }

        private AgendaPasseio MontarEntidade(AgendaPasseioModel model)
        {
            return new AgendaPasseio
            {
                Id = model.Id.HasValue ? model.Id.Value : Guid.NewGuid(),
                AgendaAceita = false,
                DataHorario = model.DataHorario,
                IdPet = model.IdPet
            };
        }

        
    }
}
