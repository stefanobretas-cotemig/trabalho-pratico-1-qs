﻿using HomePets.WebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomePets.WebApp.Domain.Interfaces
{
    public interface IUsuarioService
    {
        Task<List<Usuario>> ListarPasseadores();
        Task<NotaMediaPasseadorDto> ObterNotaMediaPasseador(Guid idPasseador);
    }
}
