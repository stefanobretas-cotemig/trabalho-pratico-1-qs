﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Domain.Models
{
    public class Passeio : Entity
    {
        public int TempoPasseio { get; set; }

        public int Nota { get; set; }

        public double ValorTotal { get; set; }

        public bool PasseioPago { get; set; }

        public Guid IdAgenda { get; set; }

        public Guid IdPasseador { get; set; }

        public virtual AgendaPasseio Agenda { get; set; }

        public virtual Usuario Passeador { get; set; }
    }
}
