﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Domain.Models
{
    public class AgendaPasseio : Entity
    {
        public DateTime DataHorario { get; set; }

        public bool AgendaAceita { get; set; }

        public Guid IdPet { get; set; }

        public virtual Pet Pet { get; set; }

        public virtual Passeio Passeio { get; set; }
    }
}
