﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Domain.Models
{
    public class Pet : Entity
    {
        public string Nome { get; set; }

        public string TipoPet { get; set; }

        public string Raca { get; set; }

        public string Observacao { get; set; }

        public Guid IdUsuario { get; set; }

        public virtual Usuario Dono { get; set; }

        public virtual List<AgendaPasseio> Agendas { get; set; }
    }
}
