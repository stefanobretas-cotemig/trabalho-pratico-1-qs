﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Domain.Models
{
    public class Usuario : Entity
    {
        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public TipoUsuario TipoUsuario { get; set; }

        public virtual List<Pet> Pets { get; set; }

        public virtual List<Passeio> Passeios { get; set; }
    }

    public enum TipoUsuario
    {
        DonoPet = 1,
        Passeador = 2
    }
}
