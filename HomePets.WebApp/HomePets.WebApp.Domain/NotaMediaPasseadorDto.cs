﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Domain
{
    public class NotaMediaPasseadorDto
    {
        public NotaMediaPasseadorDto(string nome, double notaMedia, int qtdePasseios)
        {
            Nome = nome;
            NotaMedia = notaMedia;
            QtdePasseios = qtdePasseios;
        }

        public string Nome { get; set; }

        public double NotaMedia { get; set; }

        public int QtdePasseios { get; set; }
    }
}
