﻿using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Repositories
{
    public class PetRepository : IPetRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public PetRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Pet>> ListarPets()
        {
            return await _dbContext.Pets
                .Include(x => x.Dono)
                .ToListAsync();
        }
    }
}
