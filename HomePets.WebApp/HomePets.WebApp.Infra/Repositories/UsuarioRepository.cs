﻿using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public UsuarioRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Usuario> ObterUsuarioPorId(Guid id)
        {
            var passeador = await _dbContext.Usuarios.FirstOrDefaultAsync(x => x.Id == id);
            return passeador;
        }

        public async Task<List<Passeio>> ListarPasseiosPorPasseador(Guid idPasseador)
        {
            var passeiosPorPasseador = await _dbContext.Passeios
                        .Include(x => x.Agenda)
                        .ThenInclude(x => x.Pet)
                        .Include(x => x.Passeador)
                        .Where(x => x.IdPasseador == idPasseador)
                        .ToListAsync();
            
            return passeiosPorPasseador;
        }

        public async Task<List<Usuario>> ListarPasseadores()
        {
            return await _dbContext.Usuarios
                .Where(x => x.TipoUsuario == TipoUsuario.Passeador)
                .ToListAsync();
        }

        public async Task<List<Usuario>> ListarDonos()
        {
            return await _dbContext.Usuarios
                .Where(x => x.TipoUsuario == TipoUsuario.DonoPet)
                .ToListAsync();
        }

        public async Task<bool> CadastrarUsuario(Usuario usuario)
        {
            await _dbContext.Usuarios.AddAsync(usuario);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> AtualizarUsuario(Usuario usuario)
        {
            _dbContext.Update(usuario);
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
