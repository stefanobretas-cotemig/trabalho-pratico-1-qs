﻿using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Repositories
{
    public class AgendaPasseioRepository : IAgendaPasseioRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AgendaPasseioRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<AgendaPasseio>> ListarAgendasAtivas()
        {
            var lista = await _dbContext.AgendaPasseios
                .Include(x => x.Pet)
                .Where(x => x.AgendaAceita == false)
                .ToListAsync();

            return lista;
        }

        public async Task<AgendaPasseio> BuscarAgendaPorId(Guid id)
        {
            return await _dbContext.AgendaPasseios
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> AceitarAgenda(Guid id)
        {
            var agenda = await _dbContext.AgendaPasseios
                .FirstOrDefaultAsync(x => x.Id == id);

            agenda.AgendaAceita = true;

            _dbContext.Update(agenda);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task ApagarAgenda(Guid id)
        {
            var agenda = await _dbContext.AgendaPasseios.FirstOrDefaultAsync(x => x.Id == id);
            _dbContext.AgendaPasseios.Remove(agenda);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> CadastrarAgenda(AgendaPasseio agenda)
        {
            await _dbContext.AgendaPasseios.AddAsync(agenda);
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
