﻿using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Repositories
{
    public class PasseioRepository : IPasseioRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public PasseioRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Passeio> ObterPasseioPorId(Guid id)
        {
            return await _dbContext.Passeios
                .Include(x => x.Agenda)
                .ThenInclude(x => x.Pet)
                .Include(x => x.Passeador)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Passeio>> ListarPasseiosAtivos()
        {
            var lista = await _dbContext.Passeios
                .Include(x => x.Agenda)
                .ThenInclude(x => x.Pet)
                .Include(x => x.Passeador)
                .Where(x => !x.PasseioPago)
                .ToListAsync();

            return lista;
        }

        public async Task<bool> IniciarPasseio(Passeio passeio)
        {
            _dbContext.Passeios.Add(passeio);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> AtualizarPasseio(Passeio passeio)
        {
            _dbContext.Update(passeio);
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
