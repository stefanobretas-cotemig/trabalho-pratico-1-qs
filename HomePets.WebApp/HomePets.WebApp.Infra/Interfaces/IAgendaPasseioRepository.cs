﻿using HomePets.WebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Interfaces
{
    public interface IAgendaPasseioRepository
    {
        Task<List<AgendaPasseio>> ListarAgendasAtivas();

        Task<bool> CadastrarAgenda(AgendaPasseio agenda);

        Task<bool> AceitarAgenda(Guid id);

        Task<AgendaPasseio> BuscarAgendaPorId(Guid id);

        Task ApagarAgenda(Guid id);
    }
}
