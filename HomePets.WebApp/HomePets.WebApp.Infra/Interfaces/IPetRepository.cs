﻿using HomePets.WebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Interfaces
{
    public interface IPetRepository
    {
        Task<List<Pet>> ListarPets();
    }
}
