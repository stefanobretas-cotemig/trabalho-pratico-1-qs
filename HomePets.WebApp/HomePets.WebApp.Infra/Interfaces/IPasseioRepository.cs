﻿using HomePets.WebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Interfaces
{
    public interface IPasseioRepository
    {
        Task<Passeio> ObterPasseioPorId(Guid id);

        Task<List<Passeio>> ListarPasseiosAtivos();

        Task<bool> IniciarPasseio(Passeio passeio);

        Task<bool> AtualizarPasseio(Passeio passeio);
    }
}
