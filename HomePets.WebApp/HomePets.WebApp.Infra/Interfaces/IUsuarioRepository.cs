﻿using HomePets.WebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomePets.WebApp.Infra.Interfaces
{
    public interface IUsuarioRepository
    {
        Task<List<Usuario>> ListarPasseadores();
        Task<List<Usuario>> ListarDonos();
        Task<bool> CadastrarUsuario(Usuario usuario);
        Task<bool> AtualizarUsuario(Usuario usuario);
        Task<Usuario> ObterUsuarioPorId(Guid id);
        Task<List<Passeio>> ListarPasseiosPorPasseador(Guid idPasseador);
    }
}
