﻿using HomePets.WebApp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Infra.Mappings
{
    public class AgendaPasseioMap : IEntityTypeConfiguration<AgendaPasseio>
    {
        public void Configure(EntityTypeBuilder<AgendaPasseio> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.DataHorario)
                .HasColumnType("datetime not null")
                .IsRequired();

            builder.Property(x => x.AgendaAceita)
                .IsRequired();

            builder.HasOne(x => x.Pet)
                .WithMany(x => x.Agendas)
                .HasForeignKey(x => x.IdPet);

            builder.HasOne(x => x.Passeio)
                .WithOne(p => p.Agenda)
                .HasForeignKey<Passeio>(x => x.IdAgenda);

            builder.ToTable("AgendaPasseio");
        }
    }
}
