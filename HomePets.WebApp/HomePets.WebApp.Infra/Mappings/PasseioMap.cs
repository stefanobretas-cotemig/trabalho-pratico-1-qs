﻿using HomePets.WebApp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Infra.Mappings
{
    public class PasseioMap : IEntityTypeConfiguration<Passeio>
    {
        public void Configure(EntityTypeBuilder<Passeio> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.TempoPasseio)
                .HasColumnType("int not null")
                .IsRequired();

            builder.Property(x => x.Nota)
                .HasColumnType("int not null")
                .IsRequired();

            builder.Property(x => x.ValorTotal)
                .HasColumnType("float")
                .IsRequired();

            builder.Property(x => x.PasseioPago)
                .IsRequired();

            builder.HasOne(x => x.Passeador)
                .WithMany(x => x.Passeios)
                .HasForeignKey(x => x.IdPasseador);

            builder.ToTable("Passeio");
        }
    }
}
