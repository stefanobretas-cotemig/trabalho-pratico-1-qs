﻿using HomePets.WebApp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Infra.Mappings
{
    public class PetMap : IEntityTypeConfiguration<Pet>
    {
        public void Configure(EntityTypeBuilder<Pet> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(30)")
                .IsRequired();

            builder.Property(x => x.TipoPet)
                .HasColumnType("varchar(20)")
                .IsRequired();

            builder.Property(x => x.Raca)
                .HasColumnType("varchar(30)")
                .IsRequired();

            builder.Property(x => x.Observacao)
                .HasColumnType("varchar(240)")
                .IsRequired();

            builder.HasOne(x => x.Dono)
                .WithMany(x => x.Pets)
                .HasForeignKey(x => x.IdUsuario);

            builder.ToTable("Pet");
        }
    }
}
