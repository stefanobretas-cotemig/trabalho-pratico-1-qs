﻿using HomePets.WebApp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomePets.WebApp.Infra.Mappings
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(x => x.Cpf)
                .HasColumnType("varchar(11)")
                .IsRequired();

            builder.Property(x => x.Bairro)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(x => x.Cidade)
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(x => x.Email)
                .HasColumnType("varchar(30)")
                .IsRequired();

            builder.Property(x => x.Telefone)
                .HasColumnType("varchar(12)")
                .IsRequired();

            builder.Property(x => x.TipoUsuario)
                .IsRequired();

            builder.HasMany(x => x.Pets)
                .WithOne(p => p.Dono)
                .HasForeignKey(p => p.IdUsuario);

            builder.ToTable("Usuario");
        }
    }
}
