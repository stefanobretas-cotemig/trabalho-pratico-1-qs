﻿using HomePets.WebApp.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace HomePets.WebApp.Infra
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Pet> Pets { get; set; }

        public DbSet<Passeio> Passeios { get; set; }

        public DbSet<AgendaPasseio> AgendaPasseios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var assembly = typeof(ApplicationDbContext).Assembly;
            modelBuilder.ApplyConfigurationsFromAssembly(assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
