﻿using HomePets.WebApp.Domain;
using HomePets.WebApp.Domain.Interfaces;
using HomePets.WebApp.Domain.Models;
using HomePets.WebApp.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomePets.WebApp.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public async Task<List<Usuario>> ListarPasseadores()
        {
            var result = await _usuarioRepository.ListarPasseadores();
            return result;
        }

        public async Task<NotaMediaPasseadorDto> ObterNotaMediaPasseador(Guid idPasseador)
        {
            var passeios = await _usuarioRepository.ListarPasseiosPorPasseador(idPasseador);

            if (passeios.Any())
                return new NotaMediaPasseadorDto(passeios.FirstOrDefault().Passeador.Nome, passeios.Average(x => x.Nota), passeios.Count);
            else
            {
                var passeador = await _usuarioRepository.ObterUsuarioPorId(idPasseador);
                return new NotaMediaPasseadorDto(passeador.Nome, 0, 0);
            }
        }

    }
}
